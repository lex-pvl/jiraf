$('#reesForm').validate({
	rules: {
		name: {
			required: true
		},
		mail: {
			required: true,
			email: true
		}
	},
	messages: {
		name: {
			required: 'Введите имя'
		},
		mail: {
			required: 'Введите почту',
			email: 'Введите корректную  почту'
		}
	}
});

$('#popupForm').validate({
	rules: {
		name: {
			required: true
		},
		password: {
			required: true
		}
	},
	messages: {
		name: {
			required: 'Введите имя'
		},
		password: {
			required: 'Введите пароль'
		}
	},
	submitHandler: function(form) {
		$.ajax({
			url: '',
			type: 'POST',
			data: $(form).serialize(),
			success: function (data) {
				$('.ajax-helper').addClass('active');
				$('.ajax-helper-header').css('color', 'green');
				$('.ajax-helper-status').text('Статус: ' + data.status);
				$('.ajax-helper-text').text('Вы успешно авторизовались');
				setTimeout(function(){
					$('body').removeClass('no-scroll');
					$('body').css('padding-right', 0);
					$('.overlay').removeClass('active');
					$('.popup').removeClass('active');
				}, 2000)
				setTimeout(function(){
					$('.ajax-helper').removeClass('active');
				}, 5000)
			},
			error: function(err) {
				$('.ajax-helper').addClass('active');
				$('.ajax-helper-header').css('color', 'red');
				$('.ajax-helper-status').text('Статус: ' + err.status);
				$('.ajax-helper-text').text('Ошибка сервера, повторите попытку позже');
				setTimeout(function(){
					$('body').removeClass('no-scroll');
					$('body').css('padding-right', 0);
					$('.overlay').removeClass('active');
					$('.popup').removeClass('active');
				}, 2000)
				setTimeout(function(){
					$('.ajax-helper').removeClass('active');
				}, 5000)
			}
		});
		return false;
	}
});

$('#registerForm').validate({
	rules: {
		name: {
			required: true
		},
		mail: {
			required: true,
			email: true
		},
		password: {
			required: true
		},
		repassword: {
			required: true,
			equalTo: "#password"
		}
	},
	messages: {
		name: {
			required: 'Введите имя'
		},
		mail: {
			required: 'Введите почту',
			email: 'Введите корректную  почту'
		},
		password: {
			required: 'Введите пароль'
		},
		repassword: {
			required: 'Повторите пароль',
			equalTo: 'Пароли не совпадают'
		}
	},

	submitHandler: function(form) {
		$.ajax({
			url: '',
			type: 'POST',
			data: $(form).serialize(),
			success: function (data) {
				$('.ajax-helper').addClass('active');
				$('.ajax-helper-header').css('color', 'green');
				$('.ajax-helper-status').text('Статус: ' + data.status);
				$('.ajax-helper-text').text('Регистрация прошла успешно');
				setTimeout(function(){
					$('body').removeClass('no-scroll');
					$('body').css('padding-right', 0);
					$('.overlay').removeClass('active');
					$('.popup').removeClass('active');
				}, 2000)
				setTimeout(function(){
					$('.ajax-helper').removeClass('active');
				}, 5000)
			},
			error: function(err) {
				$('.ajax-helper').addClass('active');
				$('.ajax-helper-header').css('color', 'red');
				$('.ajax-helper-status').text('Статус: ' + err.status);
				$('.ajax-helper-text').text('Ошибка сервера, повторите попытку позже');
				setTimeout(function(){
					$('body').removeClass('no-scroll');
					$('body').css('padding-right', 0);
					$('.overlay').removeClass('active');
					$('.popup').removeClass('active');
				}, 2000)
				setTimeout(function(){
					$('.ajax-helper').removeClass('active');
				}, 5000)
			}
		});
		return false;
	}
});

$('#contactsForm').validate({
	rules: {
		name: {
			required: true
		},
		mail: {
			required: true,
			email: true
		}
	},
	messages: {
		name: {
			required: 'Введите имя'
		},
		mail: {
			required: 'Введите почту',
			email: 'Введите корректную  почту'
		}
	},

	submitHandler: function(form) {
		$.ajax({
			url: '',
			type: 'GET',
			data: $(form).serialize(),
			beforeSend: function() {
				$('#contactsForm').find('.reg-btn').html('<img src="static/img/general/three-dots.svg">');
			},
			complete: function() {
				$('#contactsForm').find('.reg-btn').html('Отправить');
			},
			success: function (data, text, xhr) {
				$('.ajax-helper').addClass('active');
				$('.ajax-helper-header').css('color', 'green');
				$('.ajax-helper-status').text('Статус: ' + xhr.status);
				$('.ajax-helper-text').text('Ваша заявка принята');
				setTimeout(function(){
					$('.ajax-helper').removeClass('active');
				}, 5000);
			},
			error: function(err) {
				$('.ajax-helper').addClass('active');
				$('.ajax-helper-header').css('color', 'red');
				$('.ajax-helper-status').text('Статус: ' + err.status);
				$('.ajax-helper-text').text('Ошибка сервера, повторите попытку позже');
				setTimeout(function(){
					$('.ajax-helper').removeClass('active');
				}, 5000)
			}
		});
		return false;
	}
});

$('.ajax-helper-close').on('click', function() {
	$('.ajax-helper').removeClass('active');
});


function validatePost() {
	var form = $('.post__content form');
	var sellForm = $('.sell-form');
	form.each(function() {
		$(this).on('submit', function(event) {
			var select = $(this).find('select');
			var input = $(this).find('input[type="text"]')

			select.each(function() {
				if ($(this).val() === null) {
					$(this).next().addClass('error-select');
					event.preventDefault();
				} else {
					$(this).next().removeClass('error-select');
				}

				$(this).on('change', function() {
					if ($(this).val() === null) {
						$(this).next().addClass('error-select');
						event.preventDefault();
					} else {
						$(this).next().addClass('success-select');
					}
				});
			});

			input.each(function() {
				if ($(this).val() < 1) {
					$(this).addClass('error-value');
					event.preventDefault();
				} else {
					$(this).removeClass('error-value');
				}
			});

		});
	});

	sellForm.each(function() {
		$(this).on('submit', function(event) {
			var select = $(this).find('select');
			var input = $(this).find('input[type="text"]')

			select.each(function() {
				if ($(this).val() === null) {
					$(this).next().addClass('error-value');
					event.preventDefault();
				} else {
					$(this).next().removeClass('error-value');
				}

				$(this).on('change', function() {
					if ($(this).val() === null) {
						$(this).next().addClass('error-value');
						event.preventDefault();
					} else {
						$(this).next().removeClass('error-value');
					}
				});
			});

			input.each(function() {
				if ($(this).val() < 1) {
					$(this).addClass('error-value');
					event.preventDefault();
				} else {
					$(this).removeClass('error-value');
				}
				
				$(this).on('input', function() {
					if ($(this).val() < 1) {
						$(this).addClass('error-value');
						event.preventDefault();
					} else {
						$(this).removeClass('error-value');
					}
				});
			});

		});
	});
}

validatePost();