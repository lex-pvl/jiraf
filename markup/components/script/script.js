$(document).ready(function() {
	rating();
	customSelect();
	formTabs();
	autoFor();
	replaceSymbols();
	showPopup();
	showRegion();
	showColors();
	showMenu();
	showLang();
	format();
	fileUpload();
	PhoneMask();
	openFilters();
	fancy();
	generateMobileMenu();
	anchorLocation(); 
});

function rating() {
	$('.rating svg').each(function() {
		$(this).on('click', function() {
			$(this).css('opacity', 1);
			$(this).prevAll().css('opacity', 1);
			$(this).nextAll().css('opacity', 0.48);
		});
	});
}

function customSelect() {
	$('#sort').niceSelect();
	$('.sell-select').each(function() {
		$(this).niceSelect();
	});

	$('.info-select').each(function() {
		$(this).niceSelect();
	});
}

function formTabs() {
	$('ul.index-form-list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.index-form').find('div.index-form-content').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.popup__list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.popup').find('div.popup__body').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.sos-list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.sos-form-content').find('div.tab-tabs').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.post__header').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.post').find('div.post__content').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.obv--list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.post__content').find('div.obv--content').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.reviews__list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.reviews').find('div.reviews__body').removeClass('active').eq($(this).index()).addClass('active');
	});
}

function autoFor() {
	var labels = $('.check-label');
	labels.each(function() {
		$(this).attr('for', $(this).prev().attr('id'));
	});
}

function replaceSymbols() {
	var numberInput = $('.form-number');
	numberInput.each(function() {
		$(this).bind('change keyup input click', function() {
			if (this.value.match(/[^0-9]/g)) {
				this.value = this.value.replace(/[^0-9]/g, '');
			}
		});
	});
}

function showPopup() {
	var overlay = $('.overlay');
	var popup = $('.popup');
	var link = $('[data-popup="true"]');
	
	link.on('click', function(event) {
		event.preventDefault();
		$('body').css('padding-right', 17);
		$('body').addClass('no-scroll');
		overlay.addClass('active');
		popup.addClass('active');
	});

	overlay.on('click', function() {
		$('body').css('padding-right', 0);
		$('body').removeClass('no-scroll');
		overlay.removeClass('active');
		popup.removeClass('active');		
	});
}


function showRegion() {
	var popup = $('.region-popup');
	var overlay = $('.overlay');
	var add = $('.add-region');
	var width = $(window).width();

	var regionsArray = [];

	add.each(function() {
		$(this).on('click', function (event) {
			event.preventDefault();
			$('body').css('padding-right', 17);
			$('body').addClass('no-scroll');
			overlay.addClass('active');
			popup.addClass('active');

			var formRegions = $(this).siblings('.form-regions');
			var popupForm = $('.region-popup-form');
			var popupBtn = $('.region-popup-btn');
			var popupInput = $('.region-popup-input');

			popupBtn.on('click', function() {
				regionsArray.splice(0, regionsArray.length);
				formRegions.empty();

				popupInput.map(function(index, elem) {

					if (elem.checked) {
						formRegions.append(` 
							<div class="form-regions-item">
							<span class="remove-reg">&times;</span>
							<p>${elem.value}</p>
							</div>
							`);
					}

					$('.remove-reg').each(function() {
						$(this).on('click', function(event) {
							$(this).parents('.form-regions-item').remove();
							if ( $(this).siblings('p').text() === elem.value) {
								elem.checked = false;
							}
						});
					});

				}); 

				$('body').css('padding-right', 0);
				$('body').removeClass('no-scroll');
				overlay.removeClass('active');
				popup.removeClass('active');
			});
		});

	});

	overlay.on('click', function () {
		$('body').css('padding-right', 0);
		$('body').removeClass('no-scroll');
		overlay.removeClass('active');
		popup.removeClass('active');
	});

	

}

function showColors() {
	var pick = $('.clr-pick');
	var btn = $('.color-pick');
	var close = $('.clr-close');
	var coverlay = $('.clr-overlay');

	var clickedArray = [];

	btn.each(function() {
		$(this).on('click', function() {
			$('body').addClass('no-scroll');
			$('body').css('padding-right', 17);
			coverlay.addClass('active');
			pick.addClass('active');

			var color = pick.find('.color');
			// var added = $('.color-added'); 
			var thAdded = $(this).siblings('.color-added');
			thAdded.empty(); 
			clickedArray = [];

			pick.on('click', function(event) {
				color.each(function(index, elem) {
					if (event.target === elem ) {
						thAdded.empty();
						thAdded.append(elem.outerHTML);
						// clickedArray.push(elem.outerHTML); 
					}

					// for (var i = 0; i < clickedArray.length; i++) {
					// 	if (clickedArray.indexOf(clickedArray[i]) !== -1) {
					// 		thAdded.append(clickedArray[i]);
					// 		clickedArray = [];
					// 	}
					// }
				});

			});

		});

	});


	coverlay.on('click', function() {
		$('body').css('padding-right', 0);
		$('body').removeClass('no-scroll');
		coverlay.removeClass('active');
		pick.removeClass('active');
	});

	close.on('click', function() {
		$('body').css('padding-right', 0);
		$('body').removeClass('no-scroll');
		coverlay.removeClass('active');
		pick.removeClass('active');
	});
}

function showMenu() {
	var link = $('.header__list-item');

	link.hover(function() {
		$(this).find('ul').addClass('active');
	}, function() {
		$(this).find('ul').removeClass('active');
	});
}

function showLang() {
	var link = $('.lang__link');
	var lang = $('.change--lang');
	var footerLink = $('.footer-sb-items');
	var footerLang = $('.footer-sb .change--lang')

	link.on('click', function (event) {
		event.preventDefault();
		lang.addClass('active');
		footerLang.removeClass('active');
	});

	footerLink.on('click', function () {
		footerLang.addClass('active');
	});

	$(document).on('mousedown', function(event){
		if(footerLang.has(event.target).length === 0 && event.target !== footerLang){
			footerLang.removeClass('active');
		}
	});

	$(document).on('mousedown', function(event){
		if(lang.has(event.target).length === 0 && event.target !== lang){
			lang.removeClass('active');
		}
	});
}

setInterval(function(){
	showLang();
}, 5000)

function format() {
	var format = $('.format');

	format.each(function() {
		$(this).numberMask({
			type:'float',
			afterPoint:2,
			defaultValueInput:'',
			decimalMark:'.'
		});
	});
}


function fileUpload() {

	var fileCollection = new Array();

	$('#images').on('change', function(e) {
		var files = e.target.files;

		$.each(files, function(index, file) {

			fileCollection.push(file);
			
			var reader = new FileReader();

			reader.readAsDataURL(file);

			reader.onload = function(e) {

				var imgBox = `
				<a href="${e.target.result}" data-id="${index++}" data-fancybox="gallery">
				<img src="${e.target.result}" alt="added">
				</a>
				`

				var template = `
				<div class="img">
				<a href="${e.target.result}" class="openfancy">
				<img src="${e.target.result}" alt="added">
				</a>
				<div class="img-hover">
				<div class="zoom">
				<img src="static/img/general/zoom.svg" alt="zoom">
				</div>
				<div class="delete" id="deleteimg">
				<img src="static/img/general/delete.svg" alt="delete">
				</div>
				</div>
				</div>
				`

				$('.defaultlink').remove();
				$('.post__image').append(imgBox);
				// $('.post__image img').attr('src', e.target.result);

				$('.added-img').append(template);

				$('.zoom').each(function() {
					$(this).on('click', function(event) {
						event.preventDefault();
						$.fancybox.close();
						$.fancybox.open( $(this).closest('.img').find('a') );
					});
				});

				$('.delete').each(function() {
					$(this).on('click', function() {
						var indexImg = $(this).parents('.img').index();
						var thImg = $(this).parents('.img');
						thImg.remove();

						var imglinks = $('.post__image a');

						imglinks.map(function(index, elem) {
							if (indexImg === index) {
								elem.remove()
							}
						})


						// $('.post__image a').each(function(){
						// 	if (indexImg === $(this).index()) {
						// 		console.log(indexImg)
						// 		console.log($(this).index())
						// 		// $(this).remove();
						// 	}
						// })
					});
				});
			}

		});	

	});



	// $('.openfancy').fancybox();
}

function generateMobileMenu() {
	var windowWidth = $(window).width();
	if (windowWidth <= 991) {
		$('.header__nav').clone().appendTo('.mobile__menu')
	}

	$('.header__burger').each(function() {
		$(this).on('click', function() {
			$('body').toggleClass('no-scroll');
			$('.mobile__menu').toggleClass('active');
		});
	});

	$('.header__list-item').each(function() {
		$(this).on('click', function() {
			$(this).find('.header__sublist').slideToggle();
		});
	});
}

function PhoneMask() {
	$('.phone-mask').each(function() {
		$(this).mask("+7 (999) - 999 - 99 - 99");
	});
}

function openFilters() {
	var indexForm = $('.index-form');
	$('#openFilter').each(function() {
		$(this).on('click', function(event) {
			event.preventDefault();
			indexForm.slideToggle(400);
		});
	});
}


function fancy() {
	$('.vet-fancy').fancybox();
}

function anchorLocation() {
	
	if ('#buytwo' === window.location.hash) {
		$('#buytwo').siblings().removeClass('active');
		$('#buytwo').addClass('active');
		$('#buytwo-content').siblings().removeClass('active');
		$('#buytwo-content').addClass('active');	
	}

	if ('#accept' === window.location.hash) {
		$('#accept').siblings().removeClass('active');
		$('#accept').addClass('active');
		$('#accept-content').siblings().removeClass('active');
		$('#accept-content').addClass('active');	
	}

	if ('#vyazka' === window.location.hash) {
		$('#vyazka').siblings().removeClass('active');
		$('#vyazka').addClass('active');
		$('#vyazka-content').siblings().removeClass('active');
		$('#vyazka-content').addClass('active');	
	}

	if ('#help' === window.location.hash) {
		$('#help').siblings().removeClass('active');
		$('#help').addClass('active');
		$('#help-content').siblings().removeClass('active');
		$('#help-content').addClass('active');	
	}
}

function flexibleTextarea(){
	var _txt = document.getElementById('textarea'); 
	var _minRows = 2; 

	if (_txt) {
		function setRows() {
			_txt.rows = _minRows; 
			do {
				if (_txt.clientHeight != _txt.scrollHeight) _txt.rows += 1;
			} while (_txt.clientHeight < _txt.scrollHeight);
		}
		setRows();
		_txt.rows = _minRows;

		_txt.oninput = function(){
			setRows();
		}
	}
}
if (window.addEventListener)
	window.addEventListener("load", flexibleTextarea, false);
else if (window.attachEvent)
	window.attachEvent("onload", flexibleTextarea);
